import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HtInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // Update header for example
    let modifiedReq = req.clone({headers: req.headers.set('Accept', 'application/json')});

    // Change protocol for example
    modifiedReq = modifiedReq.clone({
      url: modifiedReq.url.replace('http://', 'https://')
    });

    return next.handle(modifiedReq);
  }
}
