import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { HttpService } from './services/http.service';

@Injectable()
export class AppEffects {
  constructor(private actions$: Actions, private http: HttpService) {}
}
