import {createAction, createFeatureSelector, createSelector, createReducer, on, props} from '@ngrx/store';

// LoadCities
export const loadCities = createAction(
  '[FormComponent] loadCities',
  props<any>()
);

export interface LoadCitiesState {
  data: object;
}

export const initialLoadCities: LoadCitiesState = {
  data: {}
};

export const loadCitiesReducer = createReducer(
  initialLoadCities,
  on(loadCities, (state,  cities) => {
    const res = [];
    Object.keys(cities).filter(k => k !== 'type').map(i => res.push(cities[i]));
    return {
      ...state,
      data: res
    };
  }));


export const loadCitiesSelector = createFeatureSelector<LoadCitiesState>('loadCities');
export const loadCitiesListSelector = createSelector(
  loadCitiesSelector,
  state => state.data
);


// AddToIndex
export const addCityToIndex = createAction(
  '[FormComponent] addCityToIndex',
  props<any>()
);

export interface AddCityToIndexState {
  data: Array<any>;
}

export const initialAddCityToIndex: AddCityToIndexState = {
  data: []
};

export const addCityToIndexReducer = createReducer(
  initialAddCityToIndex,
  on(addCityToIndex, (state, cities) => {
    const res = [];
    Object.keys(cities).filter(k => k !== 'type').map(i => res.push(cities[i]));
    return {
      ...state,
      data: res
    };
  })
);

export const addCityToIndexSelector = createFeatureSelector<AddCityToIndexState>('addCityToIndex');
export const addCityToIndexListSelector = createSelector(
  addCityToIndexSelector,
  state => state.data
);


