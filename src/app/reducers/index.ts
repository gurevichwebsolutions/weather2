import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import {AddCityToIndexState, loadCitiesReducer, LoadCitiesState, addCityToIndexReducer} from './form.reducer';

export interface State {
  loadCities: LoadCitiesState;
  addCityToIndex: AddCityToIndexState;
}

export const reducers: ActionReducerMap<State> = {
  loadCities: loadCitiesReducer,
  addCityToIndex: addCityToIndexReducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
