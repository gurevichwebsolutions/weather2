import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  private apiKey = 'c03b0c6eb7ceedb91c8f1660cb45899f';

  public getWeatherByCityName = (cityName: string): Observable<any> => {
    // http specially for interceptor
    return this.http.get(`http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&appid=${this.apiKey}`).pipe(
      map(el => {
        return {
          ...(el as any).city,
          temperature: Math.round((el as any).list[0].main.temp / 10),
          description: (el as any).list[0].weather[0].description,
          icon: (el as any).list[0].weather[0].icon
        };
      })
    );
  }

  public getCityByCoords = (position): Observable<any> => {
    const lat = position.coords.latitude;
    const lon = position.coords.longitude;
    // http specially for interceptor
    return this.http.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${this.apiKey}`);
  }
}
