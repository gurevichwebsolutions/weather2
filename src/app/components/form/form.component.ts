import { Component, OnInit } from '@angular/core';
import {loadCitiesListSelector, loadCities, addCityToIndex, addCityToIndexListSelector} from '../../reducers/form.reducer';
import cities from '/src/assets/data/cities.json';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor(private store: Store, private http: HttpService) {}

  public cityControl = new FormControl();
  public addCityToIndexListSelector$ = this.store.select(addCityToIndexListSelector);
  public loadCitiesListSelector$ = this.store.select(loadCitiesListSelector);

  ngOnInit(): void {
    this.loadCities();
    this.getWeatherForCurrentCity();
  }

  public getWeatherForCurrentCity = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.http.getCityByCoords(position).subscribe(data => {
          this.addCityToIndex(data.name);
        });
      });
    }else{
      console.log('User not allowed');
    }
  }

  public loadCities = () => {
    this.store.dispatch(loadCities(cities));
  }

  public addCityToIndex = (city) => {
    // tslint:disable-next-line:no-shadowed-variable
    let cities: any[] = [];

    this.addCityToIndexListSelector$.subscribe(data => {
        cities = data;
    });

    // If city is already exists
    if (cities.filter(el => el.name === city).length > 0) {
      return;
    }

    this.http.getWeatherByCityName(city).subscribe(data => {
      cities = Object.assign([], cities);
      cities.push(data);
      this.store.dispatch(addCityToIndex(cities));
    });
  }

  public displayFn(city: any): string {
    return city && city.name ? city.name : '';
  }
}
