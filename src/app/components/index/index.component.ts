import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {addCityToIndex, addCityToIndexListSelector} from '../../reducers/form.reducer';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  constructor(private store: Store, private http: HttpService) { }

  public addCityToIndexListSelector$ = this.store.select(addCityToIndexListSelector);

  ngOnInit(): void {
  }

  public deleteCityWeather = (cityName) => {
    let res = [];
    this.addCityToIndexListSelector$.subscribe(data => {
      res = data.filter(el => el.name !== cityName);
    });
    this.store.dispatch(addCityToIndex(res));
  }

  public refreshCityWeather = (cityName) => {
    this.http.getWeatherByCityName(cityName).subscribe(data => {
      let cities;
      let updatedCities;
      this.addCityToIndexListSelector$.subscribe(res => cities = res);
      updatedCities = cities.map(el => {
          if (el.name === cityName) {
            el = {...data,  a: 'up'}; // To check update
          }
          return el;
      });
      this.store.dispatch(addCityToIndex(updatedCities));
    });
  }

}
